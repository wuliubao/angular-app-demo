function nonVehicleCtrl($scope, $state, constantFactory, utilFactory) {
    'use strict';
    $scope.checkTypes = [{searchCondation:true,className:"add-btn",btnName:"新增",value:"",isDelete:false}];
    $scope.searchType = function(condition){
     
    };

    $scope.addCheckType = function(index,checkType){
      if(index === 0){
        $scope.checkTypes.push({searchCondation:true,className:"delete-btn",btnName:"删除",value:"",isDelete:false});
      }else{
      	$scope.checkTypes[index].isDelete = true;
      	$scope.checkTypes.splice(index,0);
      }
    };

    $scope.goSearchResultPage = function(){
        utilFactory.gotoState(constantFactory.uiRouterState.SEARCH_RESULT, {dataType: 2});
    };
   
}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('nonVehicleCtrl', nonVehicleCtrl);
} else {
    window.policyApp.registerController('nonVehicleCtrl', nonVehicleCtrl);
}