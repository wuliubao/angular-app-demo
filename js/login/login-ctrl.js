function loginCtrl($scope, $ionicPlatform, $timeout, constantFactory, logFactory, utilFactory, loginFactory) {
    'use strict';

    $scope.loginUser = {
        userName: '',
        userPwd: '',
        autologin: false
    };

    $scope.pwdChanged = false;

    /*
     * 密码输入框的状态控制
     */
    $scope.pwdControl = {
        viewUserPwd: false,
        eyeHide: true
    };

    /**
     * 应用挂起
     */
    function onPause() {
        loginFactory.pauseTime = new Date(); // 记录离开本页面时间
    }

    /**
     * 根据后台停留时间判断是否清除密码
     */
    function isClearPassword() {
        var intervelTime;
        if (loginFactory.pauseTime) {
            intervelTime = new Date() - loginFactory.pauseTime;

            if (intervelTime > 1000 * 60 * 10) {
                $scope.loginUser.userPwd = '';
                loginFactory.pauseTime = 0;
            }
        }
    }

    /**
     * 重新拉起应用
     */
    function onResume() {
        isClearPassword();
    }

    // 注册系统事件
    function registerAppmanEvent() {
      try {
        atelier.appman.onBackground.addListener(onPause);
        atelier.appman.onForeground.addListener(onResume);
        utilFactory.log("unifiedLogin: registered to appman to receive onAction、onResume events.");
      } catch (e) {}
    }

    function init() {

    }

    /**
     * 登录按钮事件
     */
    $scope.login = function() {
        $scope.pwdControl.eyeHide = true;  // 密码转为 ***
        utilFactory.gotoState(constantFactory.uiRouterState.ABOUT);
    };

    
    /**
     * 查看密码和关闭查看密码
     */
    $scope.switchViewPassword = function() {
        $scope.pwdControl.eyeHide = !$scope.pwdControl.eyeHide;
        $scope.pwdChanged = true;
    };

    /**
     * 帐号回车
     */
    $scope.onUsernameKeyup = function(e) {
        var keyCode = window.event ? e.keyCode : e.which;
        if (keyCode === constantFactory.EnterKeyCode) {
            document.getElementById("password").focus();
            //document.query
        }
    };

    /**
     * 密码回车
     */
    $scope.onPasswordKeyup = function(e) {
        var keyCode = window.event ? e.keyCode : e.which;
        if (keyCode === constantFactory.EnterKeyCode) {
            if (unifiedLoginFactory.showPopupFlag) {
                $scope.login();
                document.getElementById("password").blur();
            }
        }
    };

    /*
     * 用户名框失去焦点
     */
    $scope.accountBlur = function() {
        if ($scope.pwdChanged) {
            document.getElementById("password").focus();
        }
        $scope.pwdChanged = false;
    };

    /*
     * 密码框失去焦点
     */
    $scope.pwdBlur = function() {
        if ($scope.pwdChanged) {
            document.getElementById("password").focus();
        }
        $scope.pwdChanged = false;
    };


    $scope.$on('$stateChangeStart', function() {
    });

    $scope.$on('$ionicView.enter', function() {
        isClearPassword();
    });

    init();
    registerAppmanEvent();
}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('loginCtrl', loginCtrl);
} else {
    window.policyApp.registerController('loginCtrl', loginCtrl);
}