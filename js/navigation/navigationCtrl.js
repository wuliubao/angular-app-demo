function navigationCtrl(utilFactory, logFactory,constantFactory,$state,$scope) {
    'use strict';

    $scope.currentWorkInfo = {
        onDutyPerson:"李四",
        workingState:"巡逻",
        workingArea:"信通处巡防线路",
        guardWay:"汽车",
        startTime:"2017年02月24日13时27分00秒",
        endTime:"2017年02月24日21时27分00秒"
    };

    $scope.isServicePage = true;

    $scope.openService = function(){
        console.log("[app-ctrl] open reportState page");
        utilFactory.gotoState(constantFactory.uiRouterState.REPORT_STATE);
    };

    $scope.openNoMasterVehicle = function(){
        console.log("[app-ctrl] open noMasterVehicle page");
        utilFactory.gotoState(constantFactory.uiRouterState.VEHICLE);
    };

    $scope.openNoMasterNonVehicle = function(){
        console.log("[app-ctrl] open noMasterNonVehicle page");
        utilFactory.gotoState(constantFactory.uiRouterState.NON_VEHICLE);
    };
    $scope.openSearch = function(){
        console.log("[app-ctrl] open search page");
        utilFactory.gotoState(constantFactory.uiRouterState.SEARCH);
    };
}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('navigationCtrl', navigationCtrl);
} else {
    window.policyApp.registerController('navigationCtrl', navigationCtrl);
}