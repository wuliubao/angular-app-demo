function appCtrl (utilFactory, logFactory, constantFactory, $rootScope, $location, $state) {
    'use strict';

    var isFirstVisit = true; // 每次程序运行时第一次访问

    function registerAppmanEvent() {
        try {
            atelier.appman.onAction.addListener(onAction);
            atelier.appman.onForeground.addListener(onResume);

            logFactory.logI("appstore: registered to appman to receive onAction、 onResume events.");
        } catch (e) {}
    }

    /*初始化操作*/
    function init() {
        console.log("[app-ctrl] init");
        if (true === isFirstVisit) {
            registerAppmanEvent();
        }
        isFirstVisit = false;
    }

    // 从后台切到前台
    function onResume() {
        logFactory.logI('appstore: onResume()');
    }

    function onAction(params) {
        logFactory.logI('appstore: onAction() params: ' + utilFactory.toJsonStr(params));
    }

    init();

    //返回键监听操作
    document.addEventListener('keyup', function(e) {
        var event = e || window.event;
        if (e.which === 4) {
            if ($location.path() !== '/navigation' && $location.path() !== '') {
                $rootScope.forward = false;
            }
            else {
                event.preventDefault();
                atelier.appman.suspend();
            }
        }

    }, false);
}
if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('appCtrl', appCtrl);
} else {
    window.policyApp.registerController('appCtrl', appCtrl);
}