window.policyApp.factory('utilFactory', ['$rootScope', '$state', '$ionicPopup', '$q', '$timeout', '$ionicHistory', '$translate',
  '$ionicLoading', '$sce', 'constantFactory', 'logFactory',
  function($rootScope, $state, $ionicPopup, $q, $timeout, $ionicHistory, $translate, $ionicLoading, $sce, constantFactory, logFactory) {
    'use strict';
    $rootScope.atelierOS = {};
    var service = {};

    /* loadmask 底部黑条提示*/
    service.hintTimer = null;
    service.hintMask = null;
    service.hintMaskContainer = null;
    service.hintMaskIcon = null;
    service.loadMask = null;
    service.loadMaskContainer = null;
    service.loadMaskIcon = null;
    service.topAlign = null;
    service.hintMsgs = [];

    /* 底部黑条 */
    window.onresize = function(e) {
      $rootScope.$broadcast('resizeHandler');
      var i,
        len,
        sheets = document.styleSheets,
        styleSheet = null;

      for (i = 0, len = sheets.length; i < len; i++) {
        if (sheets[i].href && sheets[i].href.indexOf('style.css') > 0) {
          styleSheet = sheets[i];
          break;
        }
      }

      //键盘弹出后高度判断(meta7对应的高度是345，为了其他机器也适用，这里直接用小于400来控制。后期需考虑横屏竖屏的高度区别)
      if (e.target.innerHeight < 470) {
        if (styleSheet) {
          styleSheet.addRule('.byod-bottom-bar', 'display: none');
          styleSheet.addRule('.byod-byod', 'bottom: 0');
          styleSheet.addRule('.modal-backdrop, .modal-backdrop-bg', 'bottom: 0');
        }
      } else {
        if (styleSheet) {
          styleSheet.addRule('.byod-bottom-bar', 'display: block');
          styleSheet.addRule('.byod-byod', 'bottom: 2.125rem');
          styleSheet.addRule('.modal-backdrop, .modal-backdrop-bg', 'bottom: 2.125rem');
        }
      }
    };


    /**
     * 底部黑条提示
     * @param msg
     * @param type
     * @param code
     * @param topAlign
     */
    service.hint = function(msg, type, code, topAlign) {
      var msgSize, lastMsg;
      service.hideLoading();
      // 统一转换国际化
      msg = service.translate(msg);
      //特殊字符转义
      msg = msg.replace(new RegExp(/&/g, "gm"), "&amp;");
      if (this.hintTimer) {
        msgSize = service.hintMsgs.length;
        if (msgSize === 0) {
          if (service.lastHintMsg === msg) {
            return; // 不重复弹提示信息
          }
        } else if (msgSize > 0) {
          lastMsg = service.hintMsgs[service.hintMsgs.length - 1];
          if (lastMsg && lastMsg.msg === msg) {
            return; // 不添加重复消息到队列
          }
        }
        logFactory.logW('already show hint in front, push in list');
        service.hintMsgs.push({
          msg: msg,
          type: type,
          code: code,
          topAlign: topAlign
        });
        return;
      }

      this.getHintMask();
      this.hintMaskContainer.innerHTML = msg;
      service.lastHintMsg = msg;
      this.topAlign = topAlign;

      this.showHint();
      this.hintTimer = $timeout(this.onHintTimer, 3000);
    };

    service.showHint = function() {
      var lm = this.hintMask,
        top = window.innerHeight - 135; /* 调整显示位置 */

      lm.style.top = top + 'px';
    };

    service.appendHint = function(msg) {
      if (this.hintMaskContainer) {
        this.hintMaskContainer.innerHTML += msg;
      }
    };

    service.hideHint = function() {
      if (service.hintMask) {
        service.hintMask.style.top = '-200px';
        service.hintMaskContainer.innerHTML = '';
      }
    };

    service.onHintTimer = function() {
      var msgInfo = service.hintMsgs.shift();

      if (msgInfo) {
        service.hideHint();
        service.hintMaskContainer.innerHTML = msgInfo.msg;
        service.lastHintMsg = msgInfo.msg;
        service.showHint();
        service.hintTimer = $timeout(service.onHintTimer, 3000);
      } else {
        service.hintTimer = null;
        service.hideHint();
      }
    };

    service.clearHintTimer = function() {
      if (this.hintTimer) {
        this.hideHint();

        $timeout.cancel(this.hintTimer);
        this.hintTimer = null;
      }
    };

    service.getHintMask = function() {
      if (!this.hintMask) {
        this.hintMask = document.querySelector('hint-mask');
        /*this.hintMask.style.display = 'block';*/
        this.hintMaskContainer = this.hintMask.getElementsByTagName('a')[0];
      }
      return this.hintMask;
    };

    /**
     * 显示对话框
     */
    service.show = function(title, content, buttons) {
      service.hideLoading();
      $ionicPopup.show({
        content: service.translate(content),
        buttons: buttons
      });
    };

    /**
     * prompt对话框
     */
    service.showPrompt = function(subTitle, okCallback) {
      service.hideLoading();
      $ionicPopup.prompt({
        subTitle: service.translate(subTitle),
        okText: service.translate('OK')
      }).then(function(res) {
        if (undefined !== okCallback) {
          okCallback(res);
        }
      });
    };

    /**
     * 错误对话框
     */
    service.error = function(content, okCallback) {
      service.hideLoading();
      // 输出日志
      logFactory.logE(content);

      $ionicPopup.alert({
        okText: service.translate('OK'),
        content: service.translate(content)
      }).then(function() {
        if (undefined !== okCallback) {
          okCallback();
        }
      });
    };

    /**
     * 警告对话框
     */
    service.warning = function(content, okCallback, cancelCallback) {
      service.hideLoading();
      $ionicPopup.confirm({
        okText: service.translate('OK'),
        cancelText: service.translate('CANCEL'),
        content: service.translate(content)
      }).then(function(res) {
        if (res) {
          if (undefined !== okCallback) {
            okCallback();
          }
        } else {
          if (undefined !== cancelCallback) {
            cancelCallback();
          }
        }
      });
    };

    /**
     * 提示对话框
     */
    service.info = function(content, okCallback) {
      service.hideLoading();
      $ionicPopup.alert({
        okText: service.translate('OK'),
        content: service.translate(content)
      }).then(function() {
        if (undefined !== okCallback) {
          okCallback();
        }
      });
    };

    /**
     *弹窗：卸载
     */
    service.popup = function(content, okText, cancelText, okCallback, cancelCallback) {
      service.hideLoading();
      $ionicPopup.confirm({
        okText: service.translate(okText),
        cancelText: service.translate(cancelText),
        content: service.translate(content)
      }).then(function(res) {
        if (res) {
          if (undefined !== okCallback) {
            okCallback();
          }
        } else {
          if (undefined !== cancelCallback) {
            cancelCallback();
          }
        }
      });
    };

    /**
     * 显示loading加载
     */
    service.showLoading = function() {
      var loadmaskDiv = this.getLoadMask();
      /* 显示 */

      loadmaskDiv.style.zIndex = '900';
    };

    /**
     * 隐藏loading加载
     */
    service.hideLoading = function() {
      var loadmaskDiv = service.getLoadMask();
      $timeout(function() {
        loadmaskDiv.style.zIndex = '-1';
      }, 0);
    };

    service.getLoadMask = function() {
      if (!service.loadMask) {
        service.loadMask = document.querySelector('.loading-container');
      }
      return service.loadMask;
    };

    /**
     * 跳转到指定页面
     * @param param:需要跳转页面对应的state值
     * @stateParams {object}:跳转页面时所需参数对象
     */
    service.gotoState = function(param, stateParams) {
      service.hideLoading();
      param = (undefined === param) ? constantFactory.uiRouterState.LOGIN : param;

      //首页进入,不需要动画
      if (param !== constantFactory.uiRouterState.NAVIGATION) {
          $rootScope.forward = true;
      }

      logFactory.logI('goto the State:' + param);
      if (stateParams) {
          $state.go(param, stateParams);
      } else {
          $state.go(param);
      }
    };

    /**
     * 清空历史记录,禁止返回到上一个界面
     */
    service.clearHistory = function() {
      $ionicHistory.clearHistory();
    };

    /**
     * 后退到上一个页面
     */
    service.goBack = function(backCount) {
      service.hideLoading();
      if (backCount === undefined) {
        $ionicHistory.goBack();
      } else {
        $ionicHistory.goBack(backCount);
      }
    };

    /**
     * 转换为json对象
     * @param jsonStr:json对象格式化字符串
     */
    service.toJson = function(jsonStr) {
      try {
        jsonStr = jsonStr.replace(/\t/g, '  ');
        return JSON.parse(jsonStr);
      } catch (e) {
        logFactory.logE('Error: ' + e.message);
        return {};
      }
    };


    /**
     * 转换为json字符串
     * @param jsonObj:json对象
     */
    service.toJsonStr = function(jsonObj) {
      return JSON.stringify(jsonObj);
    };

    /**
     * 将内容转换为html内容
     * @param str
     * @returns {*}
     */
    service.trustAsHtml = function(str) {
      var tmpString = str.replace(/</g, '&lt;');
      tmpString = tmpString.replace(/\r\n/g, '<br>');
      tmpString = tmpString.replace(/ /g, '&nbsp;&nbsp;');
      tmpString = tmpString.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
      return $sce.trustAsHtml(tmpString).toString();
    };

    /**
     *将标签符转化为‘’，用来计算字符长度
     */
    service.trustAsnull = function(str) {
      var tmpString = str.replace(/</g, '');
      tmpString = tmpString.replace(/\r\n/g, '');
      tmpString = tmpString.replace(/ /g, '');
      tmpString = tmpString.replace(/\t/g, '');
      return $sce.trustAsHtml(tmpString).toString();
    };

    /**
     * 判断数组中是否存在该值
     * @param val:搜索的值
     * @param arr:搜寻的数组
     */
    service.isExistInArr = function(val, arr) {
      var i;
      if (null === arr || undefined === arr || 0 === arr.length) {
        return false;
      }

      for (i = 0; i < arr.length; i = i + 1) {
        if (arr[i] === val) {
          return true;
        }
      }
      return false;
    };

    /**
     * 判断两个对象是否相等
     * @param a: 对象a
     * @param b: 对象b
     */
    service.isObjectValueEqual = function(a, b) {
      if (typeof a === 'number' && typeof b === 'number') {
        return a === b;
      }

      var aProps = Object.getOwnPropertyNames(a),
        bProps = Object.getOwnPropertyNames(b);

      if (aProps.length !== bProps.length) {
        return false;
      }

      for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        if (Object.prototype.toString(a[propName]) === '[Object Object]' || Object.prototype.toString(b[propName]) === '[Object Object]') {
          if (!service.isObjectValueEqual(a[propName], b[propName])) {
            return false;
          }
        } else if (a[propName] !== b[propName]) {
          return false;
        }
      }

      return true;
    };

    /**
     * 将源对象中的属性拷贝到目标对象中
     * @param srcObj:源对象
     * @param destObj:目的对象
     * @param excludeProps:不拷贝的属性数组
     */
    service.copyPropsToDestObj = function(srcObj, destObj, excludeProps) {
      var p = "";

      if (null === srcObj || undefined === srcObj) {
        return;
      }

      destObj = (null === destObj || undefined === destObj) ? {} : destObj;

      // 开始遍历
      for (p in srcObj) {
        if (srcObj.hasOwnProperty(p)) {
          if ('function' === typeof srcObj[p] || service.isExistInArr(p, excludeProps)) {
            continue;
          }
          destObj[p] = srcObj[p];
        }
      }
    };

    /**
     * 将源对象中的属性设置到目标对象中
     * @param srcObj:源对象
     * @param destObj:目的对象
     * @param excludeProps:不拷贝的属性数组
     * @param isExcludeNullVal:是否排除空值
     */
    service.setPropsToDestObj = function(srcObj, destObj, excludeProps, isExcludeNullVal) {
      var p = "";

      if (null === srcObj || undefined === srcObj || 'object' !== typeof destObj) {
        return;
      }

      // 开始遍历
      for (p in destObj) {
        if (!srcObj.hasOwnProperty(p)) {
          continue;
        }

        if ('function' === typeof destObj[p] || service.isExistInArr(p, excludeProps)) {
          continue;
        }

        if (true === isExcludeNullVal) {
          if ('' !== srcObj[p]) {
            destObj[p] = srcObj[p];
          }
        } else {
          destObj[p] = srcObj[p];
        }
      }
    };

    /**
     * 国际化
     * @param key:需要国际化的key名称
     * @param param:国际化中参数，必须为json对象
     */
    service.translate = function(key, param) {
      if (undefined === key || '' === key) {
        return '';
      }

      if (null === param || undefined === param || 'object' !== typeof param) {
        return $translate.instant(key);
      } else {
        return $translate.instant(key, param);
      }
    };

    /* 对象clone函数 */
    service.clone = function(item) {
      var enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable',
        'toLocaleString', 'toString', 'constructor'
      ];
      if (item === null || item === undefined) {
        return item;
      }

      // DOM nodes
      if (item.nodeType && item.cloneNode) {
        return item.cloneNode(true);
      }

      // Strings
      var type = toString.call(item);

      // Dates
      if (type === '[object Date]') {
        return new Date(item.getTime());
      }

      var i, j, k, clone, key;

      // Arrays
      if (type === '[object Array]') {
        i = item.length;

        clone = [];

        while (i--) {
          clone[i] = service.clone(item[i]);
        }
      } else if (type === '[object Object]' && item.constructor === Object) { // Objects
        clone = {};

        for (key in item) {
            if(item.hasOwnProperty(key)){
              clone[key] = service.clone(item[key]);
            }
        }

        if (enumerables) {
          for (j = enumerables.length; j--;) {
            k = enumerables[j];
            clone[k] = item[k];
          }
        }
      }

      return clone || item;
    };

    /**
     * Copies all the properties of config to the specified object.
     * @param {Object} object The receiver of the properties
     * @param {Object} config The source of the properties
     * @param {Object} defaults A different object that will also be applied for default values
     * @return {Object} returns obj
     */
    service.apply = function(object, config, defaults) {
      var enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable',
        'toLocaleString', 'toString', 'constructor'
      ];
      if (defaults) {
        service.apply(object, defaults);
      }

      if (object && config && typeof config === 'object') {
        var i, j, k;

        for (i in config) {
            if (config.hasOwnProperty(i)) {
              object[i] = config[i];
            }
        }

        if (enumerables) {
          for (j = enumerables.length; j--;) {
            k = enumerables[j];
            if (config.hasOwnProperty(k)) {
              object[k] = config[k];
            }
          }
        }
      }

      return object;
    };

    /**
     * Copies all the properties of config to object if they don't already exist.
     * @param {Object} object The receiver of the properties
     * @param {Object} config The source of the properties
     * @return {Object} returns obj
     */
    service.applyIf = function(object, config) {
      var property;

      if (object) {
        for (property in config) {
          if (config.hasOwnProperty(property)) {
            if (object[property] === undefined) {
              object[property] = config[property];
            }
          }
        }
      }

      return object;
    };

    /**
     * Copies all the properties of config to object if they already exist.
     * @param {Object} object The receiver of the properties
     * @param {Object} config The source of the properties
     * @return {Object} returns obj
     */
    service.applyIfExist = function(object, config) {
      var property;

      if (object) {
        for (property in config) {
          if (config.hasOwnProperty(property) && object.hasOwnProperty(property)) {
            object[property] = config[property];
          }
        }
      }

      return object;
    };

    service.withIn = function(arr, item, bRemove, key1, key2) {
      var tmp = null,
        bRet = false,
        i,
        len;
      if (!arr) {
        return bRet;
      }

      key2 = key2 || key1;
      if (bRemove) {
        for (i = 0, len = arr.length; i < len; ++i) {
          tmp = arr.shift();
          if (key1 ? tmp[key1] === item[key2] : tmp === item) {
            bRet = tmp;
            continue;
          }
          arr.push(tmp);
        }
      } else {
        for (i = 0, len = arr.length; i < len; ++i) {
          tmp = arr[i];
          if (key1 ? tmp[key1] === item[key2] : tmp === item) {
            bRet = tmp;
            break;
          }
        }
      }
      return bRet;
    };

    /* 根据classname查找父节点链中相匹配的节点 */
    service.getTarget = function(target, cls) {
      while (target) {
        if (target.className && target.className.indexOf(cls) >= 0) {
          break;
        }
        target = target.parentNode;
      }
      return target;
    };

    return service;
  }
]);