window.policyApp.factory('logFactory', ['constantFactory', function(constantFactory) {
    'use strict';
    var service = {
        logLevel: constantFactory.logLevel.DEBUG       // 默认为info级别日志
    };

    /**
     * 输出日志
     * @param str:日志内容
     */
    service.logE = function(str) {
        if (window.console && window.console.log) {
            window.console.error(constantFactory.APP_ID + ' ' + str);
        }
    };

    service.logW = function(str) {
        if (constantFactory.logLevel.WARN <= service.logLevel) {
            if (window.console && window.console.log) {
                window.console.warn(constantFactory.APP_ID + ' ' + str);
            }
        }
    };
    
    service.logI = function(str) {
        if (constantFactory.logLevel.INFO <= service.logLevel) {
            if (window.console && window.console.log) {
                window.console.info(constantFactory.APP_ID + ' ' + str);
            }
        }
    };

    service.logD = function(str) {
        if (constantFactory.logLevel.DEBUG <= service.logLevel) {
            if (window.console && window.console.log) {
                window.console.log(constantFactory.APP_ID + ' ' + str);
            }
        }
    };

    return service;
}]);