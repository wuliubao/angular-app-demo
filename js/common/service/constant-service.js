window.policyApp.factory('constantFactory', function() {
    'use strict';
    var service = {
        EMULATOR: (chrome && chrome.anyoffice) ? false : true, // 模拟状态开关 true-开启，使用模拟数据运行 false-关闭，用于真实运行环境
        emulatorTimerInterval: 50, // 模拟状态的定时器的间隔时间
        version: '1.0.0', // 版本号
        EnterKeyCode: 13, // 回车键代码
        APP_ID: 'policyApp'
    };

    /**
     * 路由状态定义
     */
    service.uiRouterState = {
        LOGIN: 'login', // 登录页面
        ABOUT: 'about',  // 关于页面
        VEHICLE:"vehicle", //无主车辆界面
        NON_VEHICLE:"nonVehicle", //无主非机动车界面
        NAVIGATION:"navigation", //导航界面
        SEARCH: 'search', //人员盘查页面
        SEARCH_RESULT: 'searchResult',//部库数据页面
        PERSON_DETAIL: 'personDetail', //人员详情页面
        PERSON_ITEM_DETAIL: 'personItemDetail' //人员信息列表详情页面
    };

    /**
     * 日志级别
     */
    service.logLevel = {
        DEBUG: 1,
        INFO: 2,
        WARN: 3,
        ERROR: 4
    };

    return service;
});