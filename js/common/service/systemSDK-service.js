window.policyApp.factory('systemSDKFactory', ['$q', '$translate', 'constantFactory', 'logFactory',
    function($q, $translate, constantFactory, logFactory) {
    'use strict';
    var service = {},
        unavailableStr = $translate.instant('IDS_SET_AP_UNAVAILABLE');

    /**
     * 获取设备型号
     */
    service.getEquipmentName = function() {
        var deferred = $q.defer();
        logFactory.logD('begin get equipmentName.');

        try {
            atelier.systeminfo.getModel(function(result) {
                logFactory.logI('get equipmentName = ' + result);
                deferred.resolve(result || unavailableStr);
            });
        } catch (e) {
            logFactory.logE('get equipmentName error: ' + e);
            deferred.resolve('');
        }

        return deferred.promise;
    };

    /**
     * 获取系统语言
     */
    service.getSystemLanguage = function() {
        var deferred = $q.defer();
        logFactory.logD('begin get system language.');

        try {
            atelier.systemsettings.getProperty(atelier.SystemSettingsConstants.LANGUAGE_CURRENT, function(result) {
                logFactory.logI('get system language = ' + result);
                deferred.resolve(result);
            });
        } catch (e) {
            logFactory.logE('get system language error: ' + e);
            deferred.resolve(constantFactory.languageType.ZH_CN);
        }

        return deferred.promise;
    };

    /**
     * 获取内置存储单元指定模块信息
     *  memory 运行内存
     *  total 内置总存储（如16G）
     *  data 内部可用存储（除去了系统占用，如10G）     剩余空间
     *  application 应用
     *  image 图片、视频
     *  music 音乐、铃声、播客等
     *  download 下载内容
     *  cache 缓存数据
     */
    service.getFixedInfo = function() {
        var deferred = $q.defer(),
            result = {
                items: []
            };

        logFactory.logD('begin get FixedInfo.');
        try {
            atelier.storage.getInternalStorage("data", function(infos) {
                result = infos[0];
                deferred.resolve(result);
            });
        } catch (e) {
            logFactory.logE('get FixedInfo error: ' + e);
            deferred.resolve('');
        }
        return deferred.promise;
    };

    /**
     * 隐藏输入法
     */
    service.hideKeyboard = function() {
        try {
            atelier.ime.hideKeyboard(function(isSuccessed) {
                logFactory.logI("hideKeyboard result= " + isSuccessed);
            });
        } catch (e) {
            logFactory.logE('not support hideKeyboard. Error: ' + e.message);
        }
    };

    /**
     * 拉起其他的应用
     *  obj = {
     *      appid: '',
     *      uri: '',
     *      action: '',
     *      type: '',
     *      extras: {}
     *  }
     */
    service.pullAnotherApp = function(obj, time) {
        if(time){
            setTimeout(function () {
                atelier.appman.sendAction(obj);
                logFactory.logI('pull pullAnotherApp...params:' + JSON.stringify(obj) + ' timeout:' + time + 'ms');
            }, parseInt(time));
        } else {
            atelier.appman.sendAction(obj);
            logFactory.logE('pull pullAnotherApp...params:' + JSON.stringify(obj));
        }
    };

    /**
     * 拉起其他的应用
     * @param packagename: 需要拉起的应用包名
     * @param url: 拉起应用的目的地址
     * @param action: 拉起应用的包名，可用户被拉起应用返回当前应用时使用
     * @param extras: 参数
     */
    service.pullApp = function(packagename, url, action, extras) {
        var obj = {};
        obj.appid = packagename;
        obj.uri = url || '';
        obj.action = action || constantFactory.PACKAGE_NAME;
        obj.type = '';
        obj.extras = extras || {};
        service.pullAnotherApp(obj);
    };

    /**
     * 退出程序
     */
    service.exitApp = function() {
        try {
            atelier.appman.close();
        } catch (e) {
            logFactory.logE('not support appman terminated fun. Error: ' + e.message);
            window.close();
        }
    };

    return service;
}]);