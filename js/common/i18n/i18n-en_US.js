window.getEnJson = function() {
    'use strict';
    return {
        /* common */
        'TITLE': 'AnyOffice',
        'SAVE': 'Save',
        'OK': 'OK',
        'CANCEL': 'Cancel',
        'WARNING': 'Warning',
        'EXIT': 'Exit',
        'CONTINUE': 'Continue',
        'DIALOG_ERROR': 'Error',
        'DIALOG_WARNING': 'Warning',
        'DIALOG_INFO': 'Infomation'
    };
};