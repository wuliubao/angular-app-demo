window.getZhJson = function() {
    'use strict';
    return {
        /* common */
        'TITLE': 'AnyOffice',
        'SAVE': '保存',
        'OK': '确定',
        'CANCEL': '取消',
        'WARNING': '告警',
        'EXIT': '退出',
        'CONTINUE': '继续',
        'DIALOG_ERROR': '错误',
        'DIALOG_WARNING': '警告',
        'DIALOG_INFO': '提示'
    };
};