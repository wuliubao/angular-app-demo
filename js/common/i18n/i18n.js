angular
  	.module('i18n', ['pascalprecht.translate'])
  	.config(['$translateProvider', function($translateProvider) {
    	'use strict';
    	$translateProvider.useSanitizeValueStrategy(null);
    	$translateProvider.translations('zh_CN', window.getZhJson());

    	$translateProvider.translations('en_US', window.getEnJson());
  	}]);