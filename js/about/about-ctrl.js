﻿function aboutCtrl($scope, $ionicPlatform, constantFactory, utilFactory) {
    'use strict';
    $scope.data = {
        version: constantFactory.version
    };

    $scope.$on('$ionicView.enter', function() {
    });

    $scope.$on('$ionicView.leave', function() {

    });

    $scope.goBack = function() {
    };
}


if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('aboutCtrl', aboutCtrl);
} else {
    window.policyApp.registerController('aboutCtrl', aboutCtrl);
}