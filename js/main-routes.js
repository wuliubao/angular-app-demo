(function(window) {
    'use strict';
    var useSystemInstance = false;

    if (window.atelierApp) {
        window.policyApp = window.atelierApp;
        useSystemInstance = true;
    } else {
        window.policyApp = window.angular.module('policyApp', ['ionic', 'pascalprecht.translate']);
    }

    function doConfig($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider, $controllerProvider) {
        $translateProvider.useSanitizeValueStrategy(null);
        $translateProvider.translations('zh_CN', window.getZhJson());

        $translateProvider.translations('en_US', window.getEnJson());
        $translateProvider.use('zh_CN');
        //不启用缓存，否则模板dom可能不会销毁
        $ionicConfigProvider.views.maxCache(10);
        $ionicConfigProvider.views.transition('none');
        $translateProvider.use('zh_CN');

        window.policyApp.registerController = $controllerProvider.register;
        window.policyApp.resolveScriptDeps = function(dependencies) {
            return function($q, $rootScope) {
                var deferred = $q.defer();
                console.log('dependencies: ' + dependencies);
                window.$tinyload(dependencies, function() {
                    $rootScope.$apply(function() {
                        deferred.resolve();
                    });
                });
                return deferred.promise;
            };
        };

        $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'template/login.tpl.html',
            controller: 'loginCtrl',
            cache: true,
            resolve: {
                deps: window.policyApp.resolveScriptDeps([
                    'js/login/login-ctrl.js'
                ])
            }
        })
        .state('about', {
            url: '/about',
            templateUrl: 'template/about.tpl.html',
            controller: 'aboutCtrl',
            cache: false,
            resolve: {
                deps: window.policyApp.resolveScriptDeps([
                    'js/about/about-ctrl.js'
                ])
            }
        })
        .state('vehicle', {
            url: '/vehicle',
            templateUrl: 'template/vehicle.tpl.html',
            controller: 'vehicleCtrl',
            cache: false,
            resolve: {
                deps: window.policyApp.resolveScriptDeps([
                    'js/vehicle/vehicleCtrl.js',
                    'css/vehicle.css'
                ])
            }
        })
        .state('nonVehicle', {
            url: '/nonVehicle',
            templateUrl: 'template/nonVehicle.tpl.html',
            controller: 'nonVehicleCtrl',
            cache: false,
            resolve: {
                deps: window.policyApp.resolveScriptDeps([
                    'js/nonVehicle/nonVehicleCtrl.js',
                    'css/nonVehicle.css'
                ])
            }
        })
        .state('navigation', {
            url: '/navigation',
            templateUrl: 'template/navigation.tpl.html',
            controller: 'navigationCtrl',
            cache: false,
            resolve: {
                deps: window.policyApp.resolveScriptDeps([
                    'js/navigation/navigationCtrl.js',
                    'css/navigation.css'
                ])
            }
        })
        .state('search', {
                url: '/search',
                templateUrl: 'template/search.tpl.html',
                controller: 'searchCtrl',
                cache: false,
                resolve: {
                    deps: window.policyApp.resolveScriptDeps([
                        'js/search/search-ctrl.js',
                        'css/search.css'
                    ])
                }
            })
            .state('searchResult', {
                url: '/searchResult/?:dataType',
                templateUrl: 'template/search-result.tpl.html',
                controller: 'searchResultCtrl',
                cache: false,
                resolve: {
                    deps: window.policyApp.resolveScriptDeps([
                        'js/search/search-result-ctrl.js',
                        'css/search-result.css'
                    ])
                }
            })
            .state('personDetail', {
                url: '/personDetail/?:title:dataType',
                templateUrl: 'template/person-detail.tpl.html',
                controller: 'personDetailCtrl',
                cache: false,
                resolve: {
                    deps: window.policyApp.resolveScriptDeps([
                        'js/search/person-detail-ctrl.js',
                        'css/person-detail.css'
                    ])
                }
            })
            .state('personItemDetail', {
                url: '/personItemDetail/?:itemType',
                templateUrl: 'template/person-item-detail.tpl.html',
                controller: 'personItemDetailCtrl',
                cache: false,
                resolve: {
                    deps: window.policyApp.resolveScriptDeps([
                        'js/search/person-item-detail-ctrl.js',
                        'css/person-item-detail.css'
                    ])
                }
            })
        ;

        /*Default*/
       $urlRouterProvider.otherwise('/navigation');
    }

    if (useSystemInstance) {
        doConfig(window.policyApp.stateProvider, window.policyApp.urlRouterProvider, window.policyApp.ionicConfigProvider, window.policyApp.translateProvider, window.policyApp.controllerProvider);
    } else {
        window.policyApp.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider, $controllerProvider) {
            doConfig($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider, $controllerProvider);
        });
    }
})(window);