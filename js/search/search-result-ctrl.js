function searchResultCtrl(utilFactory, constantFactory, $scope, $ionicSlideBoxDelegate, $stateParams) {
    'use strict';

    var barPoint = document.querySelector("#barPoint");
    $scope.tabIndex = 0;
    $scope.dataType = $stateParams.dataType;

    $scope.searchResultTitleArray = [
        ["部库数据", "本地数据", "缉控数据"],
        ["全国请求", "本地基本", "缉控基本"],
        ["本地基本", "缉控基本"]];

    $scope.searchResultPageTitle = $scope.searchResultTitleArray[$scope.dataType][0];

    function selectTabs(index) {
        var point = index * 120;
        barPoint.style.webkitTransform = 'translateX(' + point + 'px)';
        $scope.tabIndex = index;
        $scope.searchResultPageTitle = $scope.searchResultTitleArray[$scope.dataType][index];
    }

    $scope.selectTabs = function (index) {
        selectTabs(index);
        $ionicSlideBoxDelegate.$getByHandle('dataTypeItem').slide(index);
    };

    $scope.goPersonDetailPage = function () {
        utilFactory.gotoState(constantFactory.uiRouterState.PERSON_DETAIL, {title:$scope.searchResultPageTitle,dataType:$scope.dataType});
    };

    $scope.slideHasChanged = function (index) {
        selectTabs(index);
    };


    //DUMMY数据
    $scope.searchDate = [
        ["姓名：王强","性别：男","身份证：34262519911111","详细地址：江苏省无锡市王家村三组188号"],
        ["号牌号码：苏B88888","号牌种类：轿车","中文品牌：宝马","状态：正常"],
        ["编号：123456","车辆种类：自行车","钢印号：H088888","车主姓名：王强"]]

}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('searchResultCtrl', searchResultCtrl);
} else {
    window.policyApp.registerController('searchResultCtrl', searchResultCtrl);
}