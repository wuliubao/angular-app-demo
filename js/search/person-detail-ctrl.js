function personDetailCtrl(utilFactory, constantFactory, $scope, $stateParams) {
    'use strict';

    if (!$stateParams.dataType) {
        $scope.dataType = "0";
    } else {
        $scope.dataType = $stateParams.dataType;
        $scope.personDetailPageTitle = $stateParams.title;
    }

    $scope.goPersonItemDetailPage = function (index) {
        if ($scope.dataType === "0") {
            utilFactory.gotoState(constantFactory.uiRouterState.PERSON_ITEM_DETAIL, {itemType:index});
        }
    };

    //DUMMY数据
    $scope.searchDate = [
        ["姓名：王强","性别：男", "民族：汉族","身份证：34262519911111","详细地址：江苏省无锡市王家村三组188号"],
        ["序号：1","号牌种类：小型汽车","号牌号码：苏B88888","车身颜色：灰","车辆品牌：宝马",
            "发动机型号：N88888","登记地址详情：江苏省无锡市王家村三组188号","暂住地址详情：江苏省无锡市王家村三组188号",
            "联系方式：888888","车辆总质量：1855","车辆识别代号：JIANGSHU888888888","联系方式：138888886666"],
        ["序号：1","编号：123456","车辆种类：自行车","车辆品牌： 捷安特", "车辆颜色：红色",
            "钢印号：H088888","车主姓名：王强","家庭住址：江苏省无锡市王家村三组188号","注册日期：1999-01-01 00:00:00.0"]]

}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('personDetailCtrl', personDetailCtrl);
} else {
    window.policyApp.registerController('personDetailCtrl', personDetailCtrl);
}
