function searchCtrl(utilFactory, constantFactory, $scope, $ionicSlideBoxDelegate, $ionicScrollDelegate,$state) {
    'use strict';

    var searchPageTitle = ["盘查人员（有证）","盘查人员（无证）"];



    $scope.searchPageTitle = searchPageTitle[0];
    $scope.hasID = true;

    $scope.inputDate = "";
    $scope.checkRadio = 0;

    var idNumberInputFocus = false;

    //
    $scope.searchDate = {
        base: {
            name: "",
            birthday: null,
            birthRangeStart: null,
            birthRangeEnd: null
        },
        cloud: ""
    };

    var keyEventObject = document.createEvent("KeyBoardEvent");
    // keyEventObject.initKeyEvent();

    var typeRadios = document.getElementsByName("typeRadio");
    var nameRadios = document.getElementsByName("nameRadio");
    var sexRadios = document.getElementsByName("sexRadio");

    function selectTabs(index) {
        if (index === 0) {
            $scope.hasID = true;
        } else {
            $scope.hasID = false;
        }
        $scope.searchPageTitle = searchPageTitle[index];
    }

    $scope.goSearchResultPage = function () {

        utilFactory.gotoState(constantFactory.uiRouterState.SEARCH_RESULT, {dataType: 0});
    };

    $scope.selectTabs = function (index) {
        selectTabs(index);
        $ionicSlideBoxDelegate.$getByHandle('idSelect').slide(index);
    };

    $scope.slideHasChanged = function (index) {
        selectTabs(index);
    };

    $scope.inputKeyX = function () {
        var idNumberInput = document.querySelector("#idNumberInput");
        idNumberInput.value = idNumberInput.value + "X";
        idNumberInputFocus =  true;
    };

    $scope.inputBlur = function () {
        if (idNumberInputFocus) {
            document.querySelector("#idNumberInput").focus();
        }
        idNumberInputFocus = false;
    };

    $scope.chooseType = function (index) {

        if (typeRadios[index].checked !== true) {
            typeRadios[index].checked = true;
            $scope.checkRadio = index;
            if (index !== 0) {
                for (var i = 0; i < nameRadios.length; i++) {
                    nameRadios[i].checked = false;
                }
                for (var j = 0; j < sexRadios.length; j++) {
                    sexRadios[j].checked = false;
                }
                $scope.searchDate.base = null;
            } else {
                nameRadios[0].checked = true;
                sexRadios[0].checked = true;
            }

            if (index !== 1) {
                $scope.searchDate.cloud = "";
            }
        }
    };

    var windowHeight = window.outerHeight;
    function resizeListen(e) {
        var event = e || window.event;

        $ionicScrollDelegate.$getByHandle('noneCardContent').resize();
        if (event.currentTarget.outerHeight !== windowHeight) {
            var scrollToHeight = 0;
            if (document.activeElement.id === "searchCloudInput") {
                scrollToHeight = 200;
            }
            if (scrollToHeight < 0) {
                scrollToHeight = 0;
            }
            $ionicScrollDelegate.$getByHandle('noneCardContent').scrollTo(0, scrollToHeight, true);
        }
    }

    window.addEventListener("resize", resizeListen);

    $scope.$on("$destroy", function () {
        window.removeEventListener("resize", resizeListen);
    })


}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('searchCtrl', searchCtrl);
} else {
    window.policyApp.registerController('searchCtrl', searchCtrl);
}