function personItemDetailCtrl($scope ,$stateParams) {
    'use strict';
    var personItemDetailTitle = ["缉控信息", "本地数据", "盘查记录","本地车辆","本地旅馆","本地网吧"];
    $scope.searchResultPageTitle = personItemDetailTitle[$stateParams.itemType];

    $scope.personDetail = {
    }

}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('personItemDetailCtrl', personItemDetailCtrl);
} else {
    window.policyApp.registerController('personItemDetailCtrl', personItemDetailCtrl);
}
