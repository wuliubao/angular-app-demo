function vehicleCtrl($scope, $state, utilFactory, constantFactory, $ionicModal, $ionicPlatform) {
    'use strict';
   $scope.plateTypes=["大型汽车","小型汽车","挂车","临时入境汽车",
                      "领馆汽车","拖拉机","外籍摩托车","军用车辆号牌",
                      "试验汽车","农用运输车类","两、三轮摩托车","境外摩托车",
                      "教练汽车","外籍汽车","领馆摩托车","使馆摩托车","境外汽车",
                      "教练摩托车","警用摩托车号牌","警用汽车号牌","其他号牌",
                      "轻便摩托车","使馆汽车","试验摩托车","临时行驶车","临时入境摩托车"];
   $scope.areaTypes = ["苏","津","冀","晋","蒙",
                       "辽","吉","黑","沪","浙",
                       "皖","闽","赣","鲁","豫",
                       "鄂","湘","粤","桂","琼",
                       "渝","川","贵","云","藏",
                       "陕","甘","青","宁","新",
                       "港","澳","台","京"];
   $scope.letterTypes = ["B","D","N","E","C",
                         "W","F","R","K","G",
                         "S","U","A","O","J",
                         "H","X","I","P","L",
                         "Q","T","M","V","Y",
                         "Z"];
   $scope.checkedLicense = true;
   $scope.checkedEngine = false;
   $scope.checkedFrame = false;
   $scope.plateType = $scope.plateTypes[0];
   $scope.areaType = $scope.areaTypes[0];
   $scope.letterType = $scope.letterTypes[0];
   $scope.vehicleNum = "";
   $scope.engineNum = "";
   $scope.frameNum = "";
   $scope.vehicleDisabled = false;
   $scope.engineDisabled = true;
   $scope.frameDisabled = true;

   $scope.selectType = function(type){
   	switch(type){
     case "license": {
      if(!$scope.checkedLicense){
       $scope.checkedLicense = true;
       $scope.checkedEngine = false;
       $scope.checkedFrame = false;
       $scope.engineNum = "";
       $scope.frameNum = "";
       $scope.vehicleDisabled = true;
       $scope.engineDisabled = true;
       $scope.frameDisabled = true;
      }else {
       $scope.vehicleDisabled = false;
      }
       break;
     };
     case "engine": {
      if(!$scope.checkedEngine){
  	   $scope.checkedLicense = false;
  	   $scope.checkedEngine = true;
  	   $scope.checkedFrame = false;
       $scope.vehicleNum = "";
       $scope.frameNum = "";
       $scope.vehicleDisabled = true;
       $scope.engineDisabled = true;
       $scope.frameDisabled = true;
      }else {
        $scope.engineDisabled = false;
      }
       break;
     };
     case "frame": {
      if(!$scope.checkedFrame){
  	   $scope.checkedLicense = false;
  	   $scope.checkedEngine = false;
  	   $scope.checkedFrame = true; 
       $scope.vehicleNum = "";
       $scope.engineNum = "";
       $scope.vehicleDisabled = true;
       $scope.engineDisabled = true;
       $scope.frameDisabled = true;
      }else{
        $scope.frameDisabled = false;
      }
       break;
     };
   	}
   };

  $ionicModal.fromTemplateUrl('template/vehicle-plate-type.tpl.html', {
      scope: $scope,
      animation: "slide",
      isFullScreen: true
  }).then(function(modal) {
      $scope.plateInfoModal = modal;
  });

  $ionicModal.fromTemplateUrl('template/vehicle-area-type.tpl.html', {
      scope: $scope,
      animation: "slide",
      isFullScreen: true
  }).then(function(modal) {
      $scope.areaInfoModal = modal;
  });

  $ionicModal.fromTemplateUrl('template/vehicle-letter-type.tpl.html', {
      scope: $scope,
      animation: "slide",
      isFullScreen: true
  }).then(function(modal) {
      $scope.letterInfoModal = modal;
  });

  $scope.openPlateType = function(){
    $scope.plateInfoModal.show();
  };

  $scope.openAreaType = function(){
    $scope.areaInfoModal.show();
  };

  $scope.openLetterType = function(){
    $scope.letterInfoModal.show();
  };

  $scope.goSearchResultPage = function(){
      utilFactory.gotoState(constantFactory.uiRouterState.SEARCH_RESULT, {dataType: 1});

  };

  $scope.selectPlateType = function(type){
    $scope.plateType = type;
    $scope.plateInfoModal.$el.removeClass('active');
    $scope.plateInfoModal.hide();
  };

  $scope.selectAreaType = function(type){
    $scope.areaType = type;
    $scope.areaInfoModal.$el.removeClass('active');
    $scope.areaInfoModal.hide();
  };

  $scope.selectLetterType = function(type){
    $scope.letterType = type;
    $scope.letterInfoModal.$el.removeClass('active');
    $scope.letterInfoModal.hide();
  };

  $scope.$on('modal.backdrop', function () {
   console.log("[vehicleCtrl] modal backdrop");
  });

  // var backButton = $ionicPlatform.registerBackButtonAction(function(e) {
  //   if($scope.plateInfoModal && $scope.plateInfoModal.isShown()){
  //     e.preventDefault();
  //     $scope.plateInfoModal.hide();
  //     return;
  //   }
  //   if($scope.areaInfoModal && $scope.areaInfoModal.isShown()){
  //     e.preventDefault();
  //     $scope.areaInfoModal.hide();
  //     return;
  //   }
  //   if($scope.letterInfoModal && $scope.letterInfoModal.isShown()){
  //     e.preventDefault();
  //     $scope.letterInfoModal.hide();
  //     return;
  //   }
  // },101);
  
  document.addEventListener('keyup', function(e) {
        var event = e || window.event;
        if (e.which === 4) {
          if($scope.plateInfoModal && $scope.plateInfoModal.isShown()){
            e.preventDefault();
            $scope.plateInfoModal.hide();
            return;
          }
          if($scope.areaInfoModal && $scope.areaInfoModal.isShown()){
            e.preventDefault();
            $scope.areaInfoModal.hide();
            return;
          }
          if($scope.letterInfoModal && $scope.letterInfoModal.isShown()){
            e.preventDefault();
            $scope.letterInfoModal.hide();
            return;
          }
        }
    }, false);



  $scope.$on('$destroy', function () {
    
  });
   
}

if (window.policyApp.registerController === undefined) {
    window.policyApp.controller('vehicleCtrl', vehicleCtrl);
} else {
    window.policyApp.registerController('vehicleCtrl', vehicleCtrl);
}